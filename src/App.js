import React, { Component, PropTypes } from 'react'

const style = {
  textAlign: 'center',
  fontSize: '3em',
}

export default class App extends Component {
  render() {
    return <div style={style}>Test react</div>
  }
}
